package de.kaipho.bridges;

import de.kaipho.bridges.model.Spiel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Klasse zum verteilen globaler Events.
 */
public class Event {
    private static Event ourInstance = new Event();

    public static Event getInstance() {
        return ourInstance;
    }

    private Spiel spiel;
    private List<Consumer<Spiel>> spielConsumer = new ArrayList<>();

    private List<Consumer<String>> statusConsumer = new ArrayList<>();

    private Event() {

    }

    /**
     *
     * @param consumer Consumer, der auf das Status Spiel hören soll.
     */
    public void addSpielConsumer(Consumer<Spiel> consumer) {
        this.spielConsumer.add(consumer);
    }

    /**
     *
     * @param consumer Consumer, der auf das Status Event hören soll.
     */
    public void addStatusConsumer(Consumer<String> consumer) {
        this.statusConsumer.add(consumer);
    }

    /**
     * @return das aktuell geladene Spiel.
     */
    public Spiel getSpiel() {
        return spiel;
    }

    /**
     * @param spiel Spiel, welches verteilt werden soll.
     */
    public void setSpiel(Spiel spiel) {
        this.spiel = spiel;
        spielConsumer.forEach(consumer -> consumer.accept(spiel));
    }

    /**
     * @param status Status, welcher verteilt werden soll.
     */
    public void setStatus(String status) {
        statusConsumer.forEach(consumer -> consumer.accept(status));
    }
}
