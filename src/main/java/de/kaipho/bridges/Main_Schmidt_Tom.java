package de.kaipho.bridges;

import de.kaipho.bridges.service.SpielLoeser;
import de.kaipho.bridges.ui.BridgesMenuBar;
import de.kaipho.bridges.ui.InselDrawer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

public class Main_Schmidt_Tom {

    public static void main(String[] args) {
        SpielLoeser spielLoeser = new SpielLoeser();

        JFrame mainWindow = new JFrame();
        mainWindow.setTitle("Tom Schmidt / M-Nr. 9120670");
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setMenuBar(new BridgesMenuBar());

        GridBagConstraints constraint = new GridBagConstraints();
        constraint.fill = GridBagConstraints.VERTICAL;
        constraint.gridx = 1;

        InselDrawer inselDrawer = new InselDrawer();

        JCheckBox checkbox = new JCheckBox();
        checkbox.setText("Anzahl fehlender Brücken anzeigen");
        checkbox.setPreferredSize(new Dimension(300, 45));
        checkbox.addItemListener(e -> {
            if(e.getStateChange() == ItemEvent.SELECTED) {
                inselDrawer.setVerbleibendeInselnAnzeigen(true);
            } else {
                inselDrawer.setVerbleibendeInselnAnzeigen(false);
            }
        });

        Label label = new Label();

        JPanel statusBar = new JPanel();
        statusBar.add(label);
        statusBar.setPreferredSize(new Dimension(300, 25));

        Event.getInstance().addStatusConsumer(text -> {
            label.setText(text);

            int hoehe = 160 + Event.getInstance().getSpiel().getFeldhoehe() * 30;
            int breite = Event.getInstance().getSpiel().getFeldbreite() * 30 < 300
                    ? 300
                    : Event.getInstance().getSpiel().getFeldbreite() * 30 + 30;

            mainWindow.setSize(new Dimension(breite, hoehe));
        });

        JPanel buttons = new JPanel(new GridBagLayout());
        JPanel all = new JPanel(new BorderLayout());
        JPanel bag = new JPanel(new GridBagLayout());

        Button b1 = new Button("Nächste Brücke");
        Button b2 = new Button("Automatisch lösen");
        b1.setPreferredSize(new Dimension(150, 30));
        b2.setPreferredSize(new Dimension(150, 30));

        buttons.add(b1);
        buttons.add(b2);

        b1.addActionListener(e -> {
            boolean zugGemacht = spielLoeser.macheZug(Event.getInstance().getSpiel(), false);
            if (!zugGemacht) {
                JOptionPane.showMessageDialog(null, "Kein weiterer Zug möglich!");
            }
        });

        b2.addActionListener(e -> {
            if(b2.getLabel().equals("Automatisch lösen")) {
                b2.setLabel("Lösen pausieren");

                spielLoeser.macheZuege(
                        Event.getInstance().getSpiel(),
                        s -> b2.setLabel("Automatisch lösen"),
                        s -> {
                            JOptionPane.showMessageDialog(null, "Kein weiterer Zug möglich!");
                            b2.setLabel("Automatisch lösen");
                        });


            } else {
                b2.setLabel("Automatisch lösen");

                spielLoeser.abbrechen();
            }
        });

        bag.add(checkbox, constraint);
        bag.add(buttons, constraint);

        all.add(BorderLayout.CENTER, inselDrawer);
        all.add(BorderLayout.SOUTH, bag);

        mainWindow.add(BorderLayout.CENTER, all);
        mainWindow.add(BorderLayout.SOUTH, statusBar);

        mainWindow.setSize(mainWindow.getPreferredSize());
        mainWindow.setVisible(true);
    }
}
