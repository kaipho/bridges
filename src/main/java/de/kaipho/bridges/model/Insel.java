package de.kaipho.bridges.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Eine Insel auf dem Spielfeld.
 */
public class Insel implements Comparable {
    private final Integer column;
    private final Integer row;
    private Integer bridgesCount;
    private final List<Bruecke> bridges = new ArrayList<>();

    public Insel(Integer column, Integer row, Integer bridgesCount) {
        this.column = column;
        this.row = row;
        this.bridgesCount = bridgesCount;
    }

    public Integer getColumn() {
        return column;
    }

    public Integer getRow() {
        return row;
    }

    public Integer getBridgesCount() {
        return bridgesCount;
    }

    /**
     * Zahl der noch fehlenden Brücken.
     */
    public Integer getVerbleibendeBruecken() {
        return this.getBridgesCount() - this.getBridges().size();
    }

    public List<Bruecke> getBridges() {
        return bridges;
    }

    public void setBridgesCount(Integer bridgesCount) {
        this.bridgesCount = bridgesCount;
    }

    /**
     * Kopiert das Objekt samt aller Objekte, die über Associationen verlinkt sind.
     */
    Insel kopie() {
        return new Insel(getColumn(), getRow(), getBridgesCount());
    }

    @Override
    public String toString() {
        return "Insel{" +
                "column=" + column +
                ", row=" + row +
                ", bridges=" + bridgesCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Insel insel = (Insel) o;
        return Objects.equals(column, insel.column) &&
                Objects.equals(row, insel.row);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, row, bridgesCount);
    }

    public void entferne(Bruecke bruecke) {
        getBridges().removeIf(it -> it == bruecke);
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Insel) {
            return Comparator.comparing(Insel::getColumn).thenComparing(Insel::getRow).compare(this, (Insel) o);
        }
        throw new RuntimeException(String.format("Nicht Vergleichbar! [this: %s, o: %s]", this, o));
    }
}
