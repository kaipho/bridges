package de.kaipho.bridges.model;

import de.kaipho.bridges.Event;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Das Spielfeld von Bridges.
 */
public class Spiel {

    private final Integer feldbreite;
    private final Integer feldhoehe;
    private final Integer anzahlInseln;
    private Bruecke neusteBruecke;
    private File file;

    private final List<Insel> inseln = new ArrayList<>();
    private final List<Bruecke> bruecken = new ArrayList<>();

    public Spiel(Integer feldbreite, Integer feldhoehe, Integer anzahlInseln) {
        this.feldbreite = feldbreite;
        this.feldhoehe = feldhoehe;
        this.anzahlInseln = anzahlInseln;
    }

    public Integer getFeldbreite() {
        return feldbreite;
    }

    public Integer getFeldhoehe() {
        return feldhoehe;
    }

    public Integer getAnzahlInseln() {
        return anzahlInseln;
    }

    /**
     * @return alle Inseln, deren Brückenanzahl noch nicht maximal ist.
     */
    public List<Insel> getInselnZuDenenBrueckenFehlen() {
        return getInseln().stream()
                .filter(i -> i.getVerbleibendeBruecken() > 0)
                .collect(Collectors.toList());
    }

    public List<Insel> getInseln() {
        inseln.sort(Insel::compareTo);
        return inseln;
    }

    public List<Bruecke> getBruecken() {
        Comparator<Bruecke> brueckeComparator = Comparator
                .<Bruecke, Integer>comparing(bruecke -> getInseln().indexOf(bruecke.getStartIndex()))
                .thenComparing(bruecke -> getInseln().indexOf(bruecke.getEndIndex()));
        bruecken.sort(brueckeComparator);
        return bruecken;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Entfernt (falls vorhanden) eine Brucke, die von der Insel mit den Koordinaten x, y in die gegebene Richtung führt.
     */
    public void brueckeEntfernen(int x, int y, Richtung richtung) {
        Optional<Insel> first = getInseln().stream()
                .filter(i -> i.getRow() == y && i.getColumn() == x)
                .findFirst();

        if (first.isPresent()) {
            Optional<Bruecke> bridge = first.get().getBridges().stream()
                    .filter(it -> it.getRichtung(first.get()).equals(richtung))
                    .findAny();

            bridge.ifPresent(b -> {
                getBruecken().removeIf(it -> it == bridge.get());
                if (b.isDoppelt()) {
                    getBruecken().add(b);
                }
            });
            bridge.ifPresent(Bruecke::entferne);
            neusteBruecke = null;
            ermittelZustand();
        }
    }

    /**
     * Liefert die Anzahl aller Nachbarn (mit Brücken verbindbare Inseln), die noch nicht vollständig von der Brückenanzahl her sind.
     */
    public int anzahlNichtFertigerNachbarinseln(Insel ausgangsinsel) {
        int anzahl = 0;

        if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.NORDEN) == 1
                && bestimmeNachbarinsel(ausgangsinsel, Richtung.NORDEN).get().getVerbleibendeBruecken() > 0) {
            anzahl += 1;
        }
        if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.OSTEN) == 1
                && bestimmeNachbarinsel(ausgangsinsel, Richtung.OSTEN).get().getVerbleibendeBruecken() > 0) {
            anzahl += 1;
        }
        if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.SUEDEN) == 1
                && bestimmeNachbarinsel(ausgangsinsel, Richtung.SUEDEN).get().getVerbleibendeBruecken() > 0) {
            anzahl += 1;
        }
        if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.WESTEN) == 1
                && bestimmeNachbarinsel(ausgangsinsel, Richtung.WESTEN).get().getVerbleibendeBruecken() > 0) {
            anzahl += 1;
        }

        return anzahl;
    }

    /**
     * Liefert die Anzahl der nötigen Brücken abzüglich der Brücken zu Nachbarn, die vollständig von der Brückenanzahl her sind.
     */
    public int anzahlBrueckenZuNichtFertigenNachbarinseln(Insel ausgangsinsel) {
        int anzahl = ausgangsinsel.getBridgesCount();

        anzahl -= zieheBrueckenAbFallsNachbarinselKomplett(ausgangsinsel, Richtung.NORDEN);
        anzahl -= zieheBrueckenAbFallsNachbarinselKomplett(ausgangsinsel, Richtung.OSTEN);
        anzahl -= zieheBrueckenAbFallsNachbarinselKomplett(ausgangsinsel, Richtung.SUEDEN);
        anzahl -= zieheBrueckenAbFallsNachbarinselKomplett(ausgangsinsel, Richtung.WESTEN);

        return anzahl;
    }

    private int zieheBrueckenAbFallsNachbarinselKomplett(Insel ausgangsinsel, Richtung norden) {
        if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, norden) == 1) {
            Insel nachbarinsel = bestimmeNachbarinsel(ausgangsinsel, norden).get();
            if (nachbarinsel.getVerbleibendeBruecken() == 0) {
                if (findePassendeBruecke(ausgangsinsel, nachbarinsel).get().isDoppelt()) {
                    return 2;
                } else {
                    return 1;
                }
            }
        }
        return 0;
    }

    private int bestimmeObBrueckeInRichtungMoeglich(Insel ausgangsinsel, Richtung richtung) {
        Optional<Insel> nachbarinsel = bestimmeNachbarinsel(ausgangsinsel, richtung);
        if (nachbarinsel.isPresent()) {
            if (getBruecken().stream().anyMatch(bruecke -> bruecke.verbindetBrueckeDieInseln(ausgangsinsel, nachbarinsel))) {
                return 1;
            } else if (validiereObHinzufuegenMoeglich(ausgangsinsel, richtung).isPresent()) {
                return 0;
            } else {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Prüft, ob eine Brücke von der Ausgangsinsel in die gegebene Richtung hinzugefügt werden kann.
     * Falls ja, ist das Optional leer, falls nein, findet sich darin eine Fehlermeldung.
     */
    public Optional<String> validiereObHinzufuegenMoeglich(Insel ausgangsinsel, Richtung r) {
        if (ausgangsinsel.getBridgesCount() == ausgangsinsel.getBridges().size()) {
            return Optional.of("Ausgangsinsel hat zu viele Brücken!");
        }

        Optional<Insel> nachbarinsel = bestimmeNachbarinsel(ausgangsinsel, r);

        if (!nachbarinsel.isPresent()) {
            return Optional.of("Keine Zielinsel gefunden!");
        }

        if (nachbarinsel.get().getBridgesCount() == nachbarinsel.get().getBridges().size()) {
            return Optional.of("Zielinsel hat zu viele Brücken!");
        }

        if (gibtEsEineUeberschneidung(r, ausgangsinsel, nachbarinsel.get())) {
            return Optional.of("Brücke überschneited etwas!");
        }

        Optional<Bruecke> first = findePassendeBruecke(ausgangsinsel, nachbarinsel.get());

        if (first.isPresent() && first.get().isDoppelt()) {
            return Optional.of("Brücke ist bereits doppelt!");
        }

        return Optional.empty();
    }

    /**
     * Fügt eine Brücke hinzu ausgehend von der Koordinate x/y in die gegebene Richtung hinzu.
     * Falls dies nicht möglich ist, wird eine entsprechende Meldung zurückgegeben.
     */
    public Optional<String> brueckeHinzufuegen(int x, int y, Richtung richtung) {
        Optional<Insel> ausgangsinsel = getInseln().stream()
                .filter(i -> i.getRow() == y && i.getColumn() == x)
                .findFirst();

        if (!ausgangsinsel.isPresent()) {
            neusteBruecke = null;
            return Optional.empty();
        }

        Optional<String> fehlermeldung = validiereObHinzufuegenMoeglich(ausgangsinsel.get(), richtung);
        if (fehlermeldung.isPresent()) {
            neusteBruecke = null;
            return fehlermeldung;
        }

        Optional<Insel> nachbarinsel = bestimmeNachbarinsel(ausgangsinsel.get(), richtung);
        Optional<Bruecke> first = findePassendeBruecke(ausgangsinsel.get(), nachbarinsel.get());

        if (first.isPresent()) {
            first.get().setDoppelt(true);
            neusteBruecke = first.get();
        } else {
            Bruecke bruecke = new Bruecke(ausgangsinsel.get(), nachbarinsel.get(), false);
            this.getBruecken().add(bruecke);
            neusteBruecke = bruecke;
        }

        ermittelZustand();
        return Optional.empty();
    }

    private Optional<Bruecke> findePassendeBruecke(Insel ausgangsinsel, Insel nachbarinsel) {
        return getBruecken().stream()
                .filter(it -> {
                    if (ausgangsinsel.compareTo(nachbarinsel) < 0) {
                        return it.getStartIndex().equals(ausgangsinsel) && it.getEndIndex().equals(nachbarinsel);
                    } else {
                        return it.getStartIndex().equals(nachbarinsel) && it.getEndIndex().equals(ausgangsinsel);
                    }
                })
                .findFirst();
    }

    private Optional<Insel> bestimmeNachbarinsel(Insel insel, Richtung r) {
        Optional<Insel> nachbarinsel = Optional.empty();

        int nachbarX = insel.getColumn();
        int nachbarY = insel.getRow();

        while (true) {
            switch (r) {
                case NORDEN:
                    nachbarY--;
                    break;
                case OSTEN:
                    nachbarX++;
                    break;
                case SUEDEN:
                    nachbarY++;
                    break;
                case WESTEN:
                    nachbarX--;
                    break;
            }

            if (nachbarX < 0 || nachbarX > getFeldbreite() || nachbarY < 0 || nachbarY > getFeldhoehe()) {
                break;
            }
            int finalX = nachbarX;
            int finalY = nachbarY;
            Optional<Insel> brueckensuche = getInseln().stream()
                    .filter(i -> i.getRow() == finalY && i.getColumn() == finalX)
                    .findFirst();

            if (brueckensuche.isPresent()) {
                nachbarinsel = brueckensuche;
                break;
            }
        }
        return nachbarinsel;
    }

    /**
     * Ermittelt, ob eine Brücke zwischen den beiden gegebenen Inseln eine andere Brücke schneiden würde.
     */
    public boolean gibtEsEineUeberschneidung(Richtung r, Insel ausgangsinsel, Insel nachbarinsel) {
        if (nachbarinsel == null) {
            return true;
        }

        Insel b1;
        Insel b2;

        if (ausgangsinsel.compareTo(nachbarinsel) <= 0) {
            b1 = ausgangsinsel;
            b2 = nachbarinsel;
        } else {
            b1 = nachbarinsel;
            b2 = ausgangsinsel;
        }

        if (r.equals(Richtung.NORDEN) || r.equals(Richtung.SUEDEN)) {
            // vertikal
            boolean hatUeberschneidung = getBruecken().stream()
                    .filter(Bruecke::istHorizontal)
                    .anyMatch(b -> {
                        return (b1.getRow() < b.getEndIndex().getRow() && b2.getRow() > b.getEndIndex().getRow())
                                && (b1.getColumn() > b.getStartIndex().getColumn() && b1.getColumn() < b.getEndIndex().getColumn());
                    });
            if (hatUeberschneidung) {
                neusteBruecke = null;
                return true;
            }
        } else {
            boolean hatUeberschneidung = getBruecken().stream()
                    .filter(bruecke -> !bruecke.istHorizontal())
                    .anyMatch(b -> {
                        return ((b1.getRow() > b.getStartIndex().getRow() && b2.getRow() < b.getEndIndex().getRow())
                                && (b1.getColumn() < b.getStartIndex().getColumn() && b2.getColumn() > b.getEndIndex().getColumn()));
                    });
            if (hatUeberschneidung) {
                neusteBruecke = null;
                return true;
            }
        }

        return false;
    }

    /**
     * Prüft, ob eine Brücke zwischen beiden Inseln irgendwas überlagern würde.
     */
    public boolean wirdEtwasUeberlagert(Insel ausgangsinsel, Insel nachbarinsel) {
        if (nachbarinsel == null) {
            return true;
        }

        Insel b1;
        Insel b2;

        if (ausgangsinsel.compareTo(nachbarinsel) <= 0) {
            b1 = ausgangsinsel;
            b2 = nachbarinsel;
        } else {
            b1 = nachbarinsel;
            b2 = ausgangsinsel;
        }

        if (getInseln().stream().anyMatch(in -> in.compareTo(nachbarinsel) == 0)) {
            return true;
        }

        return getBruecken().stream()
                .anyMatch(b -> {
                    Insel br1 = b.getStartIndex();
                    Insel br2 = b.getEndIndex();

                    return ((b.getStartIndex().getColumn().equals(b1.getColumn()) || b.getEndIndex().getColumn().equals(b1.getColumn())) && (
                            (br1.getRow() > b1.getRow() && br1.getRow() < b2.getRow())
                                    || (br2.getRow() > b1.getRow() && br2.getRow() < b2.getRow())
                                    || (br1.getRow() < b1.getRow() && br2.getRow() > b1.getRow())
                                    || (br1.getRow() < b2.getRow() && br2.getRow() > b2.getRow())
                    ));
                })
                ||getBruecken().stream()
                .anyMatch(b -> {
                    Insel br1 = b.getStartIndex();
                    Insel br2 = b.getEndIndex();

                    return ((b.getStartIndex().getColumn().equals(b1.getColumn()) || b.getStartIndex().getColumn().equals(b2.getColumn())) && (
                            (br1.getRow() > b1.getRow() && br1.getRow() < b2.getRow())
                                    || (br2.getRow() > b1.getRow() && br2.getRow() < b2.getRow())
                                    || (br1.getRow() < b1.getRow() && br2.getRow() > b1.getRow())
                                    || (br1.getRow() < b2.getRow() && br2.getRow() > b2.getRow())
                    ));
                })
                ||
                getBruecken().stream()
                        .anyMatch(b -> {
                            Insel br1 = b.getStartIndex();
                            Insel br2 = b.getEndIndex();

                            return ((b.getStartIndex().getRow().equals(b1.getRow()) || b.getEndIndex().getRow().equals(b1.getRow())) && (
                                    (br1.getColumn() > b1.getColumn() && br1.getColumn() < b2.getColumn())
                                            || (br2.getColumn() > b1.getColumn() && br2.getColumn() < b2.getColumn())
                                            || (br1.getColumn() < b1.getColumn() && br2.getColumn() > b1.getColumn())
                                            || (br1.getColumn() < b2.getColumn() && br2.getColumn() > b2.getColumn())
                            ));
                        })
                ||
                getBruecken().stream()
                        .anyMatch(b -> {
                            Insel br1 = b.getStartIndex();
                            Insel br2 = b.getEndIndex();

                            return ((b.getStartIndex().getRow().equals(b1.getRow()) || b.getStartIndex().getRow().equals(b2.getRow())) && (
                                    (br1.getColumn() > b1.getColumn() && br1.getColumn() < b2.getColumn())
                                            || (br2.getColumn() > b1.getColumn() && br2.getColumn() < b2.getColumn())
                                            || (br1.getColumn() < b1.getColumn() && br2.getColumn() > b1.getColumn())
                                            || (br1.getColumn() < b2.getColumn() && br2.getColumn() > b2.getColumn())
                            ));
                        });

    }

    /**
     * Kopiert das Objekt samt aller Objekte, die über Associationen verlinkt sind.
     */
    public Spiel kopie() {
        Spiel kopie = new Spiel(getFeldbreite(), getFeldhoehe(), getAnzahlInseln());
        kopie.getInseln().addAll(getInseln().stream().map(Insel::kopie).collect(Collectors.toList()));

        kopie.getBruecken().addAll(getBruecken().stream()
                .map(bruecke -> bruecke.kopie(kopie.getInseln()))
                .collect(Collectors.toList()));

        return kopie;
    }

    /**
     * getter für Brücken, der Nebenläufigkeitseffekte der Liste durch eine Kopie ausschließt, aber keine sortierung garantiert!
     */
    public List<Bruecke> getBrueckenAlsKopie() {
        return new ArrayList<>(bruecken);
    }

    /**
     * getter für Inseln, der Nebenläufigkeitseffekte der Liste durch eine Kopie ausschließt, aber keine sortierung garantiert!
     */
    public List<Insel> getInselnAlsKopie() {
        return new ArrayList<>(inseln);
    }

    public enum SPIELZUSTAND {
        GELOEST, NICHT_GELOEST, FEHLER, NICHT_LOESBAR;
    }

    /**
     * Ermittelt den Zustand, in dem sich das Spiel aktuell befindet.
     */
    public SPIELZUSTAND ermittelZustand() {
        if (getInseln().stream().allMatch(it -> it.getBridgesCount() == it.getBridges().size())) {
            if (sindAlleInselnMiteinanderVerbunden()) {
                Event.getInstance().setStatus("gelöst");
                return SPIELZUSTAND.GELOEST;
            } else {
                Event.getInstance().setStatus("enthält einen Fehler");
                return SPIELZUSTAND.FEHLER;
            }
        } else if (istKeineBrueckeMehrHinzufuegbar()) {
            Event.getInstance().setStatus("nicht mehr lösbar");
            return SPIELZUSTAND.NICHT_LOESBAR;
        } else {
            Event.getInstance().setStatus("noch nicht gelöst");
            return SPIELZUSTAND.NICHT_GELOEST;
        }
    }

    private boolean istKeineBrueckeMehrHinzufuegbar() {
        for (Insel insel : new ArrayList<>(getInseln())) {
            if (!this.validiereObHinzufuegenMoeglich(insel, Richtung.NORDEN).isPresent()
                    || !this.validiereObHinzufuegenMoeglich(insel, Richtung.OSTEN).isPresent()
                    || !this.validiereObHinzufuegenMoeglich(insel, Richtung.SUEDEN).isPresent()
                    || !this.validiereObHinzufuegenMoeglich(insel, Richtung.WESTEN).isPresent()) {
                return false;
            }
        }
        return true;
    }

    private boolean sindAlleInselnMiteinanderVerbunden() {
        HashSet<Insel> inselnRekursivErmittelt = new HashSet<>();
        sindAlleInselnMiteinanderVerbunden(getInseln().get(0), inselnRekursivErmittelt);
        return inselnRekursivErmittelt.size() == getInseln().size();
    }

    private void sindAlleInselnMiteinanderVerbunden(Insel aktuell, Set<Insel> inseln) {
        if (inseln.contains(aktuell)) {
            return;
        }
        inseln.add(aktuell);
        for (Bruecke bridge : aktuell.getBridges()) {
            if (bridge.getStartIndex().equals(aktuell)) {
                sindAlleInselnMiteinanderVerbunden(bridge.getEndIndex(), inseln);
            } else {
                sindAlleInselnMiteinanderVerbunden(bridge.getStartIndex(), inseln);
            }
        }
    }

    public Bruecke getNeusteBruecke() {
        return neusteBruecke;
    }

    /**
     * Prüft, ob die Insel direkt neben einer bestehenden Insel liegen würde.
     */
    public boolean grentztAnBestehendeInsel(Insel insel) {
        return getInseln().stream().anyMatch(i -> liegenInselnNebeneinander(insel, i));
    }

    boolean liegenInselnNebeneinander(Insel i1, Insel i2) {
        return i2.getRow().equals(i1.getRow()) && i2.getColumn().equals(i1.getColumn() + 1)
                || i2.getRow().equals(i1.getRow() + 1) && i2.getColumn().equals(i1.getColumn())
                || i2.getRow().equals(i1.getRow()) && i2.getColumn().equals(i1.getColumn() - 1)
                || i2.getRow().equals(i1.getRow() - 1) && i2.getColumn().equals(i1.getColumn());
    }

    /**
     * Erstellt eine Brücke, die problemlos plaziert werden kann.
     */
    public void erstelleSichereBruecke(Insel ausgangsinsel, boolean istDoppelt) {
        if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.NORDEN) == 1
                && sollteGemachtWerden(ausgangsinsel, Richtung.NORDEN, istDoppelt)) {
            erstelleBrueckeInRichtungMitBeachtungDoppelt(ausgangsinsel, Richtung.NORDEN);
        } else if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.OSTEN) == 1
                && sollteGemachtWerden(ausgangsinsel, Richtung.OSTEN, istDoppelt)) {
            erstelleBrueckeInRichtungMitBeachtungDoppelt(ausgangsinsel, Richtung.OSTEN);
        } else if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.SUEDEN) == 1
                && sollteGemachtWerden(ausgangsinsel, Richtung.SUEDEN, istDoppelt)) {
            erstelleBrueckeInRichtungMitBeachtungDoppelt(ausgangsinsel, Richtung.SUEDEN);
        } else if (bestimmeObBrueckeInRichtungMoeglich(ausgangsinsel, Richtung.WESTEN) == 1
                && sollteGemachtWerden(ausgangsinsel, Richtung.WESTEN, istDoppelt)) {
            erstelleBrueckeInRichtungMitBeachtungDoppelt(ausgangsinsel, Richtung.WESTEN);
        }
    }

    private boolean sollteGemachtWerden(Insel ausgangsinsel, Richtung richtung, boolean istDoppelt) {
        Optional<Insel> nachbarinsel = bestimmeNachbarinsel(ausgangsinsel, richtung);

        if (nachbarinsel.get().getVerbleibendeBruecken() == 0) {
            return false;
        }

        Optional<Bruecke> gefundeneBruecke = getBruecken().stream()
                .filter(bruecke -> bruecke.verbindetBrueckeDieInseln(ausgangsinsel, nachbarinsel))
                .findAny();

        if (gefundeneBruecke.isPresent()) {
            if (gefundeneBruecke.get().isDoppelt()) {
                return false;
            }
            if (!gefundeneBruecke.get().isDoppelt() && istDoppelt) {
                return true;
            }
            return false;
        }
        return true;
    }

    private void erstelleBrueckeInRichtungMitBeachtungDoppelt(Insel ausgangsinsel, Richtung richtung) {
        brueckeHinzufuegen(ausgangsinsel.getColumn(), ausgangsinsel.getRow(), richtung);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Spiel spiel = (Spiel) o;
        return Objects.equals(feldbreite, spiel.feldbreite) &&
                Objects.equals(feldhoehe, spiel.feldhoehe) &&
                Objects.equals(inseln, spiel.inseln) &&
                Objects.equals(bruecken, spiel.bruecken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(feldbreite, feldhoehe, inseln, bruecken);
    }

    @Override
    public String toString() {
        return "Spiel{" +
                "feldbreite=" + feldbreite +
                ", feldhoehe=" + feldhoehe +
                ", inseln=" + inseln +
                ", bruecken=" + bruecken +
                '}';
    }
}
