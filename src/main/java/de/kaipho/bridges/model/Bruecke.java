package de.kaipho.bridges.model;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Brücke, verbindet zwei Inseln, kann doppelt sein.
 */
public class Bruecke {
    private final Insel startIndex;
    private final Insel endIndex;
    private boolean doppelt;

    public Bruecke(Insel startIndex, Insel endIndex, boolean doppelt) {
        if (startIndex.compareTo(endIndex) <= 0) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        } else {
            this.startIndex = endIndex;
            this.endIndex = startIndex;
        }
        this.doppelt = doppelt;

        startIndex.getBridges().add(this);
        endIndex.getBridges().add(this);

        if (doppelt) {
            startIndex.getBridges().add(this);
            endIndex.getBridges().add(this);
        }
    }

    public void setDoppelt(boolean doppelt) {
        this.doppelt = doppelt;
        startIndex.getBridges().add(this);
        endIndex.getBridges().add(this);
    }

    public Insel getStartIndex() {
        return startIndex;
    }

    public Insel getEndIndex() {
        return endIndex;
    }

    public boolean isDoppelt() {
        return doppelt;
    }

    public boolean istHorizontal() {
        return getStartIndex().getRow().equals(getEndIndex().getRow());
    }

    /**
     * Ermittelt, in welche Richtung die Brücke ausgehend von der Ausgangsinsel geht.
     */
    public Richtung getRichtung(Insel ausgangspunkt) {
        Insel andere = ausgangspunkt.equals(getStartIndex()) ? getEndIndex() : getStartIndex();
        if (ausgangspunkt.getRow().equals(andere.getRow())) {
            if (ausgangspunkt.getColumn() < andere.getColumn()) {
                return Richtung.OSTEN;
            }
            if (ausgangspunkt.getColumn() > andere.getColumn()) {
                return Richtung.WESTEN;
            }
        }
        if (ausgangspunkt.getColumn().equals(andere.getColumn())) {
            if (ausgangspunkt.getRow() < andere.getRow()) {
                return Richtung.SUEDEN;
            }
            if (ausgangspunkt.getRow() > andere.getRow()) {
                return Richtung.NORDEN;
            }
        }
        throw new RuntimeException();
    }

    /**
     * Prüft, ob die Insel die beiden übergebenen Brücken miteinander verbindet.
     */
    public boolean verbindetBrueckeDieInseln(Insel ausgangsinsel, Optional<Insel> nachbarinsel) {
        return (this.getStartIndex().equals(ausgangsinsel) && this.getEndIndex().equals(nachbarinsel.get()))
                || (this.getStartIndex().equals(nachbarinsel.get()) && this.getEndIndex().equals(ausgangsinsel));
    }

    /**
     * Kopiert das Objekt samt aller Objekte, die über Associationen verlinkt sind.
     */
    Bruecke kopie(List<Insel> inseln) {
        int inselStartIndex = inseln.indexOf(getStartIndex());
        int inselEndIndex = inseln.indexOf(getEndIndex());

        return new Bruecke(inseln.get(inselStartIndex), inseln.get(inselEndIndex), isDoppelt());
    }

    /**
     * Entfernt die Brücke von den Inseln, die verbunden werden.
     * Falls die Brücke doppelt ist, wird eine wieder hinzugefügt, die nicht doppelt ist.
     */
    public void entferne() {
        getStartIndex().entferne(this);
        getEndIndex().entferne(this);

        if (this.isDoppelt()) {
            getStartIndex().getBridges().add(this);
            getEndIndex().getBridges().add(this);
            this.doppelt = false;
        }
    }

    @Override
    public String toString() {
        return "Bruecke{" +
                "startIndex=" + startIndex +
                ", endIndex=" + endIndex +
                ", doppelt=" + doppelt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bruecke bruecke = (Bruecke) o;
        return doppelt == bruecke.doppelt &&
                Objects.equals(startIndex, bruecke.startIndex) &&
                Objects.equals(endIndex, bruecke.endIndex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startIndex, endIndex, doppelt);
    }
}
