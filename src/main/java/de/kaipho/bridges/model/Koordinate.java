package de.kaipho.bridges.model;

import java.util.Random;

public class Koordinate {
    private int column;
    private int row;
    private Spiel spiel;

    private static final Random random = new Random();

    public Koordinate(int column, int row, Spiel spiel) {
        this.column = column;
        this.row = row;
        this.spiel = spiel;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Verschiebt die Koordinate in die gegebene Richtung. Beachtet dabei die Größe des Spielfeldes.
     * Die Verschiebung wird desto kleine, desto mehr Inseln auf dem Spielfeld platz finden sollen.
     */
    public void verschiebeInRichtung(Richtung richtung, Spiel spiel) {
        switch (richtung) {
            case NORDEN: {
                if (this.getRow() <= 1) {
                    return;
                }
                this.setRow(this.getRow() - random.nextInt(maximalerAbstand(this.getRow() - 1)));
                return;
            }
            case OSTEN: {
                if (this.getColumn() >= spiel.getFeldbreite() - 2) {
                    return;
                }
                this.setColumn(random.nextInt(maximalerAbstand(spiel.getFeldbreite() - this.getColumn() - 2)) + this.getColumn() + 2);
                return;
            }
            case SUEDEN: {
                if (this.getRow() >= spiel.getFeldhoehe() - 2) {
                    return;
                }
                this.setRow(random.nextInt(maximalerAbstand(spiel.getFeldhoehe() - this.getRow() - 2)) + this.getRow() + 2);
                return;
            }
            case WESTEN: {
                if (this.getColumn() <= 1) {
                    return;
                }
                this.setColumn(this.getColumn() - random.nextInt(maximalerAbstand(this.getColumn() - 1)));
                return;
            }
        }
    }


    private int maximalerAbstand(int row) {
        int maximalerAbstand = ((spiel.getFeldbreite() * spiel.getFeldhoehe()) / spiel.getAnzahlInseln()) / 2;
        return Math.min(maximalerAbstand, row);
    }

    @Override
    public String toString() {
        return "Koordinate{" +
                "column=" + column +
                ", row=" + row +
                '}';
    }
}
