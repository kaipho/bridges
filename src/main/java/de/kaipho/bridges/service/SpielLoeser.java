package de.kaipho.bridges.service;

import de.kaipho.bridges.Event;
import de.kaipho.bridges.model.Insel;
import de.kaipho.bridges.model.Richtung;
import de.kaipho.bridges.model.Spiel;

import java.util.Optional;
import java.util.function.Consumer;

/**
 *  Logik zum automatischen Lösen des Spiels.
 */
public class SpielLoeser {

    private boolean weitermachen;

    /**
     * Macht einen Zug, nach welchem das Spiel weiterhin lösbar ist.
     *
     * Falls kein solcher Zug möglich ist, wird false zurüchgegeben.
     *
     * @param spiel Das zu lösende Spiel
     * @param aufKopie Gibt an, ob eine Aktualisierung stattfinden soll
     */
    public boolean macheZug(Spiel spiel, boolean aufKopie) {
        if (spiel.ermittelZustand().equals(Spiel.SPIELZUSTAND.FEHLER) ||
                spiel.ermittelZustand().equals(Spiel.SPIELZUSTAND.NICHT_LOESBAR)) {
            return false;
        }

        boolean zugGemacht = versucheSicherenZug(spiel);

        if (!zugGemacht) {
            zugGemacht = macheUnsicherenZug(spiel);
        }

        if (!aufKopie) {
            Event.getInstance().setSpiel(spiel);
        }
        return zugGemacht;
    }

    /**
     * Macht automatisch ein Zug unter unter folgenden Annahmen:
     * <p>
     * 1: Anzahl Rest-Nachbarn: n <br>
     * 2.1: Rest-Brücken: 2n -> Sichere Brücken pro Richtung: 2 <br>
     * 2.2: Rest-Brücken: 2n - 1 -> Sichere Brücken pro Richtung: 1
     * </p>
     * Terminiert, sobald eine Brücke hinzugefügt wurde mit true, falls keine Brücke hinzugefügt werden konnte, mit false.
     *
     * @param spiel Das zu lösende Spiel
     */
    private boolean versucheSicherenZug(Spiel spiel) {
        for (Insel insel : spiel.getInselnZuDenenBrueckenFehlen()) {

            int nachbarn = spiel.anzahlNichtFertigerNachbarinseln(insel);
            int bruecken = spiel.anzahlBrueckenZuNichtFertigenNachbarinseln(insel);

            if (nachbarn < 1) {
                continue;
            }

            if ((2 * nachbarn) == bruecken ) {
                spiel.erstelleSichereBruecke(insel, true);
                return true;
            } else if ((2 * nachbarn - 1) == bruecken && insel.getVerbleibendeBruecken() >= nachbarn) {
                spiel.erstelleSichereBruecke(insel, false);
                return true;
            }
        }
        return false;
    }

    /**
     * Macht automatisch ein Zug, dabei wird die zu setzende Insel zufällig ermittelt.
     * Garantiert dabei, dass das Rätsel weiter lösbar bleibt. D.h. es wird einmal versucht, das Rätsel zu Ende zu lösen.
     * <br>
     * Terminiert, sobald eine Brücke hinzugefügt wurde mit true, falls keine Brücke hinzugefügt werden konnte, mit false.
     *
     * @param spiel Das zu lösende Spiel
     */
    private boolean macheUnsicherenZug(Spiel spiel) {

        for (Insel insel : spiel.getInselnZuDenenBrueckenFehlen()) {
            for (Richtung richtung : Richtung.values()) {
                Spiel kopieVomSpiel = spiel.kopie();
                Optional<String> fehler = kopieVomSpiel.validiereObHinzufuegenMoeglich(insel, richtung);

                if (!fehler.isPresent()) {
                    kopieVomSpiel.brueckeHinzufuegen(insel.getColumn(), insel.getRow(), richtung);
                    while (macheZug(kopieVomSpiel, true));

                    if (kopieVomSpiel.ermittelZustand().equals(Spiel.SPIELZUSTAND.GELOEST)) {
                        spiel.brueckeHinzufuegen(insel.getColumn(), insel.getRow(), richtung);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Macht Züge, bis das Spiel gelöst oder nicht mehr lösbar ist.
     *
     * @param spiel Das zu lösende Spiel
     * @param wennGeloest Callback, falls Spiel gelöst
     * @param wennNichtLoesbar Callback, falls Spiel nicht mehr lösbar
     */
    public void macheZuege(Spiel spiel, Consumer<Spiel> wennGeloest, Consumer<Spiel> wennNichtLoesbar) {
        weitermachen = true;
        Thread t = new Thread(() -> macheZuegeAsync(spiel, wennGeloest, wennNichtLoesbar));
        t.start();
    }

    private void macheZuegeAsync(Spiel spiel, Consumer<Spiel> wenGeloest, Consumer<Spiel> wennNichtLoesbar) {
        while (weitermachen) {

            boolean zugGemacht = macheZug(spiel, false);
            if (!zugGemacht) {
                wennNichtLoesbar.accept(spiel);
                return;
            }
            if (spiel.ermittelZustand().equals(Spiel.SPIELZUSTAND.GELOEST)) {
                wenGeloest.accept(spiel);
                return;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                wennNichtLoesbar.accept(spiel);
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Bricht den aktuellen Lösungsvorgang nach dem nächsten Zug ab.
     */
    public void abbrechen() {
        weitermachen = false;
    }
}
