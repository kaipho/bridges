package de.kaipho.bridges.service;

import de.kaipho.bridges.model.Bruecke;
import de.kaipho.bridges.model.Insel;
import de.kaipho.bridges.model.Koordinate;
import de.kaipho.bridges.model.Richtung;
import de.kaipho.bridges.model.Spiel;

import java.util.Random;

/**
 *  Logik zum generieren eines neues Spiels.
 */
public class SpielGenerator {

    private final Random random = new Random();

    /**
     * Erzeugt ein neues Spiel mit gegebener Höhe / Breite, aber zufälliger Inselanzahl.
     */
    public Spiel generiereSpiel(int hoehe, int breite) {
        int minIntervall = Math.min(hoehe, breite);
        int inseln = random.nextInt((hoehe * breite / 5) - minIntervall + 1) + minIntervall;
        return generiereSpiel(hoehe, breite, inseln);
    }

    /**
     * Erzeugt ein neues Spiel mit gegebener Höhe / Breite und Inselanzahl.
     */
    public Spiel generiereSpiel(int hoehe, int breite, int anzahlInseln) {
        if ((hoehe < 4 || hoehe > 25) || (breite < 4 || breite > 25)) {
            throw new IllegalArgumentException("\uD835\uDC35, \uD835\uDC3B ∉ [4; 25]!");
        }
        if (anzahlInseln < 2 || anzahlInseln > 0.2 * breite * hoehe) {
            throw new IllegalArgumentException("\uD835\uDC3C ∉ [2; 0,2 ∗ \uD835\uDC35 ∗ \uD835\uDC3B]");
        }

        Spiel spiel = erstelleSpielMitInselnUndBruecken(hoehe, breite, anzahlInseln, 0);

        spiel.getBruecken().clear();
        spiel.getInseln().forEach(insel -> insel.getBridges().clear());

        return spiel;
    }

    private Spiel erstelleSpielMitInselnUndBruecken(int hoehe, int breite, int anzahlInseln, int tries) {
        try {
            Spiel spiel = new Spiel(breite, hoehe, anzahlInseln);

            generiereErsteInsel(spiel);
            generiereNaechsteInsel(spiel, ermittleNaechteMoeglicheBruecke(spiel, 0));

            for (int i = 2; i < anzahlInseln; i++) {
                generiereNaechsteInsel(spiel, ermittleNaechteMoeglicheBruecke(spiel, 0));
            }
            return spiel;
        } catch (GeneratorOverrunException stackOverflowError) {
            if(tries < 1000) {
                return erstelleSpielMitInselnUndBruecken(hoehe, breite, anzahlInseln, tries + 1);
            } else {
                throw new RuntimeException("Kann keine Lösung in 1000 Versuchen finden.");
            }
        }
    }

    private void generiereNaechsteInsel(Spiel spiel, Bruecke naechsteBruecke) {
        spiel.getBruecken().add(naechsteBruecke);
        if (spiel.getInseln().contains(naechsteBruecke.getStartIndex())) {
            spiel.getInseln().add(naechsteBruecke.getEndIndex());
        } else {
            spiel.getInseln().add(naechsteBruecke.getStartIndex());
        }
    }

    private Bruecke ermittleNaechteMoeglicheBruecke(Spiel spiel, Integer tries) {
        if(tries > 100) {
            throw new GeneratorOverrunException();
        }
        Insel betrachteteInsel = ermittleNaechsteAusgangsInsel(spiel, 0);

        Insel zielinsel;
        Richtung richtung;

        Koordinate koordinate = new Koordinate(betrachteteInsel.getColumn(), betrachteteInsel.getRow(), spiel);
        richtung = Richtung.values()[random.nextInt(4)];
        koordinate.verschiebeInRichtung(richtung, spiel);


        zielinsel = new Insel(koordinate.getColumn(), koordinate.getRow(), random.nextInt(2) + 1);

        if (spiel.gibtEsEineUeberschneidung(richtung, betrachteteInsel, zielinsel)
                || spiel.wirdEtwasUeberlagert(betrachteteInsel, zielinsel)
                || spiel.grentztAnBestehendeInsel(zielinsel)) {

            return ermittleNaechteMoeglicheBruecke(spiel, tries + 1);
        }
        betrachteteInsel.setBridgesCount(betrachteteInsel.getBridgesCount() + zielinsel.getBridgesCount());
        return new Bruecke(betrachteteInsel, zielinsel, zielinsel.getBridgesCount() == 2);
    }

    private Insel ermittleNaechsteAusgangsInsel(Spiel spiel, Integer tries) {
        if(tries > 100) {
            throw new GeneratorOverrunException();
        }
        Insel betrachteteInsel = spiel.getInseln().get(random.nextInt(spiel.getInseln().size()));
        if (!kannZuWeitererInselVerbundenWerden(betrachteteInsel, spiel)) {
            return ermittleNaechsteAusgangsInsel(spiel, tries + 1);
        }
        return betrachteteInsel;
    }

    private boolean kannZuWeitererInselVerbundenWerden(Insel betrachteteInsel, Spiel spiel) {
        int maximalVerbindbareSeiten = 4;

        Insel nachNorden = new Insel(betrachteteInsel.getColumn(), betrachteteInsel.getRow() - 2, 1);
        if (betrachteteInsel.getRow() <= 0) {
            maximalVerbindbareSeiten--;
        } else if (spiel.gibtEsEineUeberschneidung(Richtung.NORDEN, betrachteteInsel, nachNorden)) {
            maximalVerbindbareSeiten--;
        }
        Insel nachSueden = new Insel(betrachteteInsel.getColumn(), betrachteteInsel.getRow() + 2, 1);
        if (betrachteteInsel.getRow() >= spiel.getFeldhoehe()) {
            maximalVerbindbareSeiten--;
        } else if (spiel.gibtEsEineUeberschneidung(Richtung.SUEDEN, betrachteteInsel, nachSueden)) {
            maximalVerbindbareSeiten--;
        }
        Insel nachOsten = new Insel(betrachteteInsel.getColumn() + 2, betrachteteInsel.getRow(), 1);
        if (betrachteteInsel.getColumn() >= spiel.getFeldbreite()) {
            maximalVerbindbareSeiten--;
        } else if (spiel.gibtEsEineUeberschneidung(Richtung.OSTEN, betrachteteInsel, nachOsten)) {
            maximalVerbindbareSeiten--;
        }
        Insel nachwesten = new Insel(betrachteteInsel.getColumn() - 2, betrachteteInsel.getRow(), 1);
        if (betrachteteInsel.getColumn() <= 0) {
            maximalVerbindbareSeiten--;
        } else if (spiel.gibtEsEineUeberschneidung(Richtung.WESTEN, betrachteteInsel, nachwesten)) {
            maximalVerbindbareSeiten--;
        }

        if (betrachteteInsel.getBridges().size() >= maximalVerbindbareSeiten) {
            return false;
        }
        return true;
    }

    private void generiereErsteInsel(Spiel spiel) {
        int column = random.nextInt(spiel.getFeldbreite());
        int row = random.nextInt(spiel.getFeldhoehe());
        Insel insel = new Insel(column, row, 0);
        spiel.getInseln().add(insel);
    }
}
