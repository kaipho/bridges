package de.kaipho.bridges.ui;

import de.kaipho.bridges.Event;
import de.kaipho.bridges.io.FileService;
import de.kaipho.bridges.model.Spiel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class BridgesMenuBar extends MenuBar {

    private FileService fileService = new FileService();
    private File zuletztBenutzt = Paths.get("/Users/neo/privat/bridges/src/test/resources/").toFile();

    public BridgesMenuBar() {
        super();

        MenuItem neuesRaetsel = new MenuItem();
        neuesRaetsel.setLabel("Neues Rätsel");
        neuesRaetsel.addActionListener(e -> {
            InselGenerator generator = new InselGenerator();
            generator.setVisible(true);
        });

        MenuItem raetselNeustarten = new MenuItem();
        raetselNeustarten.setLabel("Rätsel neu starten");
        raetselNeustarten.addActionListener(e -> {
            Spiel spiel = Event.getInstance().getSpiel();
            spiel.getBruecken().clear();
            spiel.getInseln().forEach(it -> it.getBridges().clear());
            Event.getInstance().setSpiel(spiel);
            spiel.ermittelZustand();
        });

        MenuItem raetselLaden = new MenuItem();
        raetselLaden.setLabel("Rätsel laden");
        raetselLaden.addActionListener(e -> {
            final JFileChooser fc = new JFileChooser(zuletztBenutzt);
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                try {
                    Spiel spiel = fileService.ladeSpiel(fc.getSelectedFile());
                    Event.getInstance().setSpiel(spiel);
                    spiel.ermittelZustand();
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, "Fehler beim Laden!: " + e1.getMessage());
                }
            }
        });

        MenuItem raetselSpeichern = new MenuItem();
        raetselSpeichern.setLabel("Rätsel speichern");
        raetselSpeichern.addActionListener(e -> {
            if (Event.getInstance().getSpiel() == null) {
                JOptionPane.showMessageDialog(null, "Kein Spiel vorhanden!");
            } else if (Event.getInstance().getSpiel().getFile() == null) {
                speichernUnter(e);
            } else {
                try {
                    fileService.speicherSpiel(Event.getInstance().getSpiel(), Event.getInstance().getSpiel().getFile());
                    JOptionPane.showMessageDialog(null, "Gespeichert!");
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(null, "Fehler beim Speichern!");
                }
            }
        });

        MenuItem raetselSpeichernUnter = new MenuItem();
        raetselSpeichernUnter.setLabel("Rätsel speichern unter");
        raetselSpeichernUnter.addActionListener(this::speichernUnter);


        MenuItem beenden = new MenuItem();
        beenden.setLabel("Beenden");
        beenden.addActionListener(e -> System.exit(0));

        Menu menu = new Menu();
        menu.setLabel("Datei");
        menu.add(neuesRaetsel);
        menu.add(raetselNeustarten);
        menu.add(raetselLaden);
        menu.add(raetselSpeichern);
        menu.add(raetselSpeichernUnter);
        menu.add(beenden);

        this.add(menu);
    }

    private void speichernUnter(ActionEvent actionEvent) {
        if (Event.getInstance().getSpiel() == null) {
            JOptionPane.showMessageDialog(null, "Kein Spiel vorhanden!");
        } else {
            final JFileChooser fc = new JFileChooser(zuletztBenutzt);
            int returnVal = fc.showSaveDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                try {
                    fileService.speicherSpiel(Event.getInstance().getSpiel(), fc.getSelectedFile());
                    JOptionPane.showMessageDialog(null, "Gespeichert!");
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(null, "Fehler beim Speichern!");
                }
            }
        }
    }
}
