package de.kaipho.bridges.ui;

import de.kaipho.bridges.Event;
import de.kaipho.bridges.model.Bruecke;
import de.kaipho.bridges.model.Insel;
import de.kaipho.bridges.model.Richtung;
import de.kaipho.bridges.model.Spiel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Optional;

public class InselDrawer extends JPanel {

    private int fixedPadding = 30;
    private int offset = 30;
    private Spiel spiel;
    private boolean verbleibendeInselnAnzeigen = false;

    public InselDrawer() {
        Event.getInstance().addSpielConsumer(this::redraw);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Point point = e.getPoint();

                double x1 = point.getX() - offset + 30;
                int x = (int) x1;
                int modulo = (int) x1 % fixedPadding;
                if (modulo < 15) {
                    x = (int) x1 - fixedPadding;
                } else {
                    modulo = modulo - fixedPadding;
                }
                int inselX = x / fixedPadding;

                int y = (int) point.getY();
                int moduloy = (int) point.getY() % fixedPadding;
                if (moduloy < 15) {
                    y = (int) point.getY() - fixedPadding;
                } else {
                    moduloy = moduloy - fixedPadding;
                }
                int inselY = y / fixedPadding;

                Richtung r = null;
                if (modulo > 0 && (Math.abs(moduloy) < Math.abs(modulo))) {
                    r = Richtung.OSTEN;
                } else if (modulo < 0 && (Math.abs(moduloy) < Math.abs(modulo))) {
                    r = Richtung.WESTEN;
                } else if (moduloy > 0 && (Math.abs(modulo) < Math.abs(moduloy))) {
                    r = Richtung.SUEDEN;
                } else if (moduloy < 0 && (Math.abs(modulo) < Math.abs(moduloy))) {
                    r = Richtung.NORDEN;
                }

                if (e.getButton() == 3) {
                    Event.getInstance().getSpiel().brueckeEntfernen(inselX, inselY, r);
                }
                if (e.getButton() == 1) {
                    Optional<String> message = Event.getInstance().getSpiel().brueckeHinzufuegen(inselX, inselY, r);

                    message.ifPresent(s -> JOptionPane.showMessageDialog(InselDrawer.this, s));
                }

                repaint();
            }
        });
    }

    private void redraw(Spiel spiel) {
        this.spiel = spiel;
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D drawer = (Graphics2D) g;
        drawer.setColor(Color.WHITE);
        drawer.fillRect(0, 0, this.getWidth(), this.getHeight());

        if (spiel == null) {
            return;
        }

        int istBreite = (spiel.getFeldbreite() * fixedPadding) + fixedPadding;


        offset = 30;
        if (istBreite < 300) {
            offset = ((300 - istBreite) / 2) + 30;
        }

        drawer.setColor(new Color(235, 235, 235));
        // horizontale Linien
        for (int i = 1; i <= spiel.getFeldhoehe(); i++) {
            drawer.drawLine(offset, i * fixedPadding, this.getWidth() - offset - 30, i * fixedPadding);
        }
        // vertikale Linien
        for (int i = 0; i < spiel.getFeldbreite(); i++) {
            drawer.drawLine(i * fixedPadding + offset, fixedPadding, i * fixedPadding + offset, this.getHeight() - fixedPadding);
        }

        for (Bruecke bruecke : spiel.getBrueckenAlsKopie()) {

            int xStart = (bruecke.getStartIndex().getColumn() * fixedPadding) + offset;
            int yStart = (bruecke.getStartIndex().getRow() * fixedPadding) + fixedPadding;

            int xEnd = (bruecke.getEndIndex().getColumn() * fixedPadding) + offset;
            int yEnd = (bruecke.getEndIndex().getRow() * fixedPadding) + fixedPadding;

            if (bruecke.equals(spiel.getNeusteBruecke())) {
                drawer.setColor(Color.RED);
            } else {
                drawer.setColor(Color.BLACK);
            }
            if (!bruecke.isDoppelt()) {
                drawer.drawLine(xStart, yStart, xEnd, yEnd);
            } else {
                if (bruecke.istHorizontal()) {
                    drawer.drawLine(xStart, yStart - 4, xEnd, yEnd - 4);
                    drawer.setColor(Color.BLACK);
                    drawer.drawLine(xStart, yStart + 4, xEnd, yEnd + 4);
                } else {
                    drawer.drawLine(xStart - 4, yStart, xEnd - 4, yEnd);
                    drawer.setColor(Color.BLACK);
                    drawer.drawLine(xStart + 4, yStart, xEnd + 4, yEnd);
                }
            }
        }

        for (Insel insel : spiel.getInselnAlsKopie()) {
            int x = insel.getColumn() * fixedPadding + offset;
            int y = insel.getRow() * fixedPadding + fixedPadding;

            if (insel.getBridges().size() == insel.getBridgesCount()) {
                drawer.setColor(new Color(78, 150, 78));
                drawer.fillOval(x - 12, y - 12, 24, 24);
            } else {
                drawer.setColor(Color.GRAY);
                drawer.fillOval(x - 12, y - 12, 24, 24);
            }
            drawer.setColor(Color.WHITE);
            if (isVerbleibendeInselnAnzeigen()) {
                drawer.drawString(Integer.toString(insel.getVerbleibendeBruecken()), x - 3, y + 5);
            } else {
                drawer.drawString(insel.getBridgesCount().toString(), x - 3, y + 5);
            }

            drawer.setColor(new Color(0, 0, 0, 1));
            Polygon polygon = new Polygon(new int[]{x, x + 12, x + 12}, new int[]{y, y - 12, y + 12}, 3);
            drawer.draw(polygon);
        }

        this.setSize(new Dimension(
                (spiel.getFeldbreite() * fixedPadding) + offset * 2,
                (spiel.getFeldhoehe() * fixedPadding) + fixedPadding));
    }

    public boolean isVerbleibendeInselnAnzeigen() {
        return verbleibendeInselnAnzeigen;
    }

    public void setVerbleibendeInselnAnzeigen(boolean verbleibendeInselnAnzeigen) {
        this.verbleibendeInselnAnzeigen = verbleibendeInselnAnzeigen;
        this.repaint();
    }
}


