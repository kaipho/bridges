package de.kaipho.bridges.ui;

import de.kaipho.bridges.Event;
import de.kaipho.bridges.model.Spiel;
import de.kaipho.bridges.service.SpielGenerator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Random;

public class InselGenerator extends JFrame {

    public InselGenerator() throws HeadlessException {
        super("Neues Rätsel");
        this.setLayout(new GridBagLayout());

        GridBagConstraints constraint = new GridBagConstraints();
        constraint.fill = GridBagConstraints.VERTICAL;
        constraint.gridx = 1;

        JRadioButton checkBoxAutomatisch = new JRadioButton("Automatische Größe und Inselanzahl");
        checkBoxAutomatisch.setSelected(true);

        JRadioButton checkBoxManuell = new JRadioButton("Größe und / oder Inselzahl selbst festlegen");

        checkBoxAutomatisch.addActionListener(e -> {
            checkBoxManuell.setSelected(false);
        });
        checkBoxManuell.addActionListener(e -> {
            checkBoxAutomatisch.setSelected(false);
        });

        JPanel breite = new JPanel(new GridBagLayout());
        breite.add(new JLabel("Breite: "));
        JTextField breiteTF = new JTextField("20", 6);
        breite.add(breiteTF);

        JPanel hoehe = new JPanel(new GridBagLayout());
        hoehe.add(new JLabel("Höhe: "));
        JTextField hoeheTF = new JTextField("20", 6);
        hoehe.add(hoeheTF);

        JCheckBox checkBoxInselManuell = new JCheckBox("Inselanzahl festlegen");

        JPanel inseln = new JPanel(new GridBagLayout());
        inseln.add(new JLabel("Inseln: "));
        JTextField inselnTF = new JTextField("50", 6);
        inseln.add(inselnTF);

        JPanel buttons = new JPanel(new GridBagLayout());

        JButton abbrechen = new JButton("Abbrechen");
        abbrechen.setSize(120, 30);
        abbrechen.addActionListener(e -> InselGenerator.this.setVisible(false));

        JButton ok = new JButton("OK");
        ok.setSize(120, 30);
        ok.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SpielGenerator spielGenerator = new SpielGenerator();
                try {
                    Random r = new Random();
                    Spiel s;
                    if(checkBoxAutomatisch.isSelected()) {
                        s = spielGenerator.generiereSpiel(r.nextInt(22) + 4, r.nextInt(22) + 4);
                    } else if(!checkBoxInselManuell.isSelected()) {
                        int hoeheInt = Integer.parseInt(hoeheTF.getText());
                        int breiteInt = Integer.parseInt(breiteTF.getText());
                        s = spielGenerator.generiereSpiel(hoeheInt, breiteInt);
                    } else {
                        s = spielGenerator.generiereSpiel(
                                Integer.valueOf(hoeheTF.getText()),
                                Integer.valueOf(breiteTF.getText()),
                                Integer.valueOf(inselnTF.getText()));
                    }

                    Event.getInstance().setSpiel(s);
                    s.ermittelZustand();
                    InselGenerator.this.setVisible(false);
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(InselGenerator.this, exc.getMessage());
                }
            }
        });

        buttons.add(abbrechen);
        buttons.add(ok);

        this.add(checkBoxAutomatisch, constraint);
        this.add(checkBoxManuell, constraint);
        this.add(breite, constraint);
        this.add(hoehe, constraint);
        this.add(checkBoxInselManuell, constraint);
        this.add(inseln, constraint);
        this.add(buttons, constraint);

        this.setSize(500, 220);
    }
}
