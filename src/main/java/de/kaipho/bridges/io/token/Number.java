package de.kaipho.bridges.io.token;

public class Number implements Token {

    private final Integer number;

    public Number(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return number.toString();
    }
}
