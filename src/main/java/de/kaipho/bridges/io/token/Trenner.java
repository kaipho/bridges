package de.kaipho.bridges.io.token;

public class Trenner implements Token {

    @Override
    public String toString() {
        return "|";
    }
}
