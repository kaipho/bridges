package de.kaipho.bridges.io.token;

public class Komma implements Token {

    @Override
    public String toString() {
        return ",";
    }
}
