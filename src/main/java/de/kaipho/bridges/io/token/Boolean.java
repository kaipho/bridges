package de.kaipho.bridges.io.token;

public class Boolean implements Token {

    private final boolean aBoolean;

    public Boolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public boolean getBoolean() {
        return aBoolean;
    }

    @Override
    public String toString() {
        return aBoolean + "";
    }
}
