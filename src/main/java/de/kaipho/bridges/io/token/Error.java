package de.kaipho.bridges.io.token;

public class Error implements Token {

    private final String text;

    public Error(String text) {
        this.text = text;
    }
}
