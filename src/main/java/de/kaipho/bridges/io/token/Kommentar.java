package de.kaipho.bridges.io.token;

public class Kommentar implements Token {

    private final String text;

    public Kommentar(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
