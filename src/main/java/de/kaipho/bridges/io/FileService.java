package de.kaipho.bridges.io;

import de.kaipho.bridges.io.token.Boolean;
import de.kaipho.bridges.io.token.Bridges;
import de.kaipho.bridges.io.token.Error;
import de.kaipho.bridges.io.token.Field;
import de.kaipho.bridges.io.token.Islands;
import de.kaipho.bridges.io.token.KlammerOffen;
import de.kaipho.bridges.io.token.KlammerZu;
import de.kaipho.bridges.io.token.Komma;
import de.kaipho.bridges.io.token.Kommentar;
import de.kaipho.bridges.io.token.Multiplizitaet;
import de.kaipho.bridges.io.token.Number;
import de.kaipho.bridges.io.token.Token;
import de.kaipho.bridges.io.token.Trenner;
import de.kaipho.bridges.model.Bruecke;
import de.kaipho.bridges.model.Insel;
import de.kaipho.bridges.model.Spiel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FileService {

    /**
     * Läd die Datei und transformiert sie in ein Spiel.
     */
    public Spiel ladeSpiel(File file) throws IOException {
        List<String> zeilen = Files.readAllLines(file.toPath());
        List<Token> token = erstelleTokenListe(zeilen);
        Spiel spiel = parseTokenListe(token);
        checkSemantik(spiel);
        spiel.setFile(file);
        return spiel;
    }

    List<Token> erstelleTokenListe(List<String> zeilen) {
        List<Token> token = new ArrayList<>();

        for (String zeile : zeilen) {
            if (zeile.startsWith("#")) {
                token.add(new Kommentar(zeile));
            } else if (zeile.equals("FIELD")) {
                token.add(new Field());
            } else if (zeile.equals("ISLANDS")) {
                token.add(new Islands());
            } else if (zeile.equals("BRIDGES")) {
                token.add(new Bridges());
            } else {
                List<Integer> collect = zeile.chars().boxed().collect(Collectors.toList());
                while (!collect.isEmpty()) {
                    Integer c = collect.remove(0);
                    if (Character.isDigit(c)) {
                        StringBuilder number = new StringBuilder().append(Character.getNumericValue(c));
                        while (!collect.isEmpty() && Character.isDigit(collect.get(0))) {
                            number.append(Character.getNumericValue(collect.get(0)));
                            collect.remove(0);
                        }
                        token.add(new Number(Integer.valueOf(number.toString())));
                    } else if (c == '(') {
                        token.add(new KlammerOffen());
                    } else if (c == ')') {
                        token.add(new KlammerZu());
                    } else if (c == ',') {
                        token.add(new Komma());
                    } else if (c == 'x') {
                        token.add(new Multiplizitaet());
                    } else if (c == '|') {
                        token.add(new Trenner());
                    } else if (c == 't') {
                        if (collect.subList(0, 3).equals(Arrays.asList((int) 'r', (int) 'u', (int) 'e'))) {
                            token.add(new Boolean(true));
                        } else {
                            token.add(new Error(zeile));
                            break;
                        }
                        collect.remove(0);
                        collect.remove(0);
                        collect.remove(0);
                    } else if (c == 'f') {
                        if (collect.subList(0, 4).equals(Arrays.asList((int) 'a', (int) 'l', (int) 's', (int) 'e'))) {
                            token.add(new Boolean(false));
                        } else {
                            token.add(new Error(zeile));
                            break;
                        }
                        collect.remove(0);
                        collect.remove(0);
                        collect.remove(0);
                        collect.remove(0);
                    } else if (c == ' ') {
                        // nichts tun
                    } else {
                        token.add(new Error(zeile));
                    }
                }
            }
        }

        return token;
    }

    Spiel parseTokenListe(List<Token> token) {
        token.removeIf(t -> t instanceof Kommentar);

        getAsType(token, Field.class);
        Number width = getAsType(token, Number.class);
        getAsType(token, Multiplizitaet.class);
        Number height = getAsType(token, Number.class);
        getAsType(token, Trenner.class);
        Number anzahlInseln = getAsType(token, Number.class);

        Spiel spiel = new Spiel(width.getNumber(), height.getNumber(), anzahlInseln.getNumber());
        erstelleInseln(spiel, token);

        if (!token.isEmpty()) {
            throw new RuntimeException(String.format("EOF erwartet, aber %s erhalten", token));
        }

        return spiel;
    }

    private void erstelleInseln(Spiel spiel, List<Token> token) {
        getAsType(token, Islands.class);
        List<Insel> inseln = spiel.getInseln();

        while (!token.isEmpty() && !(token.get(0) instanceof Bridges)) {

            getAsType(token, KlammerOffen.class);
            Number column = getAsType(token, Number.class);
            getAsType(token, Komma.class);
            Number row = getAsType(token, Number.class);
            getAsType(token, Trenner.class);
            Number bridges = getAsType(token, Number.class);
            getAsType(token, KlammerZu.class);

            inseln.add(new Insel(column.getNumber(), row.getNumber(), bridges.getNumber()));
        }

        erstelleBruecken(spiel, token);
    }

    private void erstelleBruecken(Spiel spiel, List<Token> token) {
        getAsTypeOptional(token, Bridges.class);
        List<Bruecke> bruecken = spiel.getBruecken();

        while (!token.isEmpty()) {

            getAsType(token, KlammerOffen.class);
            Number startIndex = getAsType(token, Number.class);
            getAsType(token, Komma.class);
            Number endIndex = getAsType(token, Number.class);
            getAsType(token, Trenner.class);
            Boolean doppelteBruecke = getAsType(token, Boolean.class);
            getAsType(token, KlammerZu.class);

            bruecken.add(new Bruecke(
                    spiel.getInseln().get(startIndex.getNumber()),
                    spiel.getInseln().get(endIndex.getNumber()),
                    doppelteBruecke.getBoolean()));
        }
    }

    private void checkSemantik(Spiel spiel) {

        spiel.getInseln().forEach(i -> {
            if (i.getRow() < 0 || i.getRow() >= spiel.getFeldhoehe()) {
                throw new RuntimeException("Insel liegt nicht im Feld: " + i);
            }
            if (i.getColumn() < 0 || i.getColumn() >= spiel.getFeldbreite()) {
                throw new RuntimeException("Insel liegt nicht im Feld: " + i);
            }
        });

    }

    private <T> T getAsType(List<Token> token, Class<T> clazz) {
        if (!token.isEmpty() && token.get(0).getClass().isAssignableFrom(clazz)) {
            return (T) token.remove(0);
        }
        throw new RuntimeException(String.format("%s erwartet, aber %s erhalten!", clazz.getSimpleName(), token));
    }

    private <T> Optional<T> getAsTypeOptional(List<Token> token, Class<T> clazz) {
        if (token.isEmpty()) return Optional.empty();
        if (token.get(0).getClass().isAssignableFrom(clazz)) {
            return Optional.of((T) token.remove(0));
        }
        throw new RuntimeException(String.format("%s erwartet, aber %s erhalten!", clazz.getSimpleName(), token));
    }

    /**
     * Speichert das Spiel in die gegebene Datei.
     */
    public void speicherSpiel(Spiel spiel, File file) throws IOException {
        List<String> lines = new ArrayList<>();
        lines.add(new Field().toString());
        lines.add(spiel.getFeldbreite() + " x " + spiel.getFeldhoehe() + " | " + spiel.getInseln().size());

        lines.add(new Islands().toString());

        spiel.getInseln().stream()
                .sorted(Comparator.comparing(Insel::getColumn).thenComparing(Insel::getRow))
                .map(insel -> String.format("( %d, %d | %d )", insel.getColumn(), insel.getRow(), insel.getBridgesCount()))
                .forEach(lines::add);

        lines.add(new Bridges().toString());

        spiel.getBruecken().stream()
                .map(bruecke -> String.format("( %s, %s | %s )",
                        spiel.getInseln().indexOf(bruecke.getStartIndex()),
                        spiel.getInseln().indexOf(bruecke.getEndIndex()),
                        bruecke.isDoppelt()))
                .forEach(lines::add);

        Files.write(file.toPath(), lines);
        spiel.setFile(file);
    }

}
