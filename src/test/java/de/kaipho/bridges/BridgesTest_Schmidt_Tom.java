package de.kaipho.bridges;

import de.kaipho.bridges.io.FileService;
import de.kaipho.bridges.model.Spiel;
import de.kaipho.bridges.service.SpielGenerator;
import de.kaipho.bridges.service.SpielLoeser;

import java.io.IOException;
import java.nio.file.Paths;

public class BridgesTest_Schmidt_Tom extends BridgesTest {


	@Override
	public BridgesTester getBridgesTesterImpl() {
		return new BridgesTester() {
			@Override
			public void testGeneratePuzzle(String filePath, int width, int height, int isles) {
				SpielGenerator spielGenerator = new SpielGenerator();
				Spiel spiel = spielGenerator.generiereSpiel(height, width, isles);
				FileService fileService = new FileService();
				try {
					fileService.speicherSpiel(spiel, Paths.get(filePath).toFile());
				} catch (IOException e) {
					throw new RuntimeException("Speichern nicht möglich!", e);
				}
			}

			@Override
			public void testSolvePuzzle(String puzzlePath, String solutionPath) {
				FileService fileService = new FileService();

				try {
					Spiel spiel = fileService.ladeSpiel(Paths.get(puzzlePath).toFile());
					Event.getInstance().setSpiel(spiel);

					SpielLoeser spielLoeser = new SpielLoeser();

					spielLoeser.macheZug(spiel, true);
					while (spiel.getInseln().stream().anyMatch(it -> it.getVerbleibendeBruecken() != 0)) {
						spielLoeser.macheZug(spiel, true);
					}

					fileService.speicherSpiel(spiel, Paths.get(solutionPath).toFile());
				} catch (IOException e) {
					throw new RuntimeException("Lösen nicht möglich!", e);
				}
			}
		};
	}

	@Override
	public String getMatrNr() {
		return "9120670";
	}

	@Override
	public String getName() {
		return "Tom Schmidt";
	}

	@Override
	public String getEmail() {
		return "schmidt@kaipho.de";
	}
}