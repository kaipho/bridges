package de.kaipho.bridges.io;

import de.kaipho.bridges.io.token.Boolean;
import de.kaipho.bridges.io.token.Bridges;
import de.kaipho.bridges.io.token.Field;
import de.kaipho.bridges.io.token.Islands;
import de.kaipho.bridges.io.token.KlammerOffen;
import de.kaipho.bridges.io.token.KlammerZu;
import de.kaipho.bridges.io.token.Komma;
import de.kaipho.bridges.io.token.Multiplizitaet;
import de.kaipho.bridges.io.token.Number;
import de.kaipho.bridges.io.token.Token;
import de.kaipho.bridges.io.token.Trenner;
import de.kaipho.bridges.model.Bruecke;
import de.kaipho.bridges.model.Insel;
import de.kaipho.bridges.model.Spiel;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class FileServiceTest {

    private FileService fileService = new FileService();

    @Test
    public void erstelleTokenListeOhneFehler() {
        InputStream resourceAsStream = FileServiceTest.class.getResourceAsStream("/bsp_14x14.sol.bgs");
        List<String> zeilen = new BufferedReader(new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.toList());

        List<Token> token = fileService.erstelleTokenListe(zeilen);

    }

    @Test
    public void testeParsenVonTokenListe() {
        // arrange
        List<Token> token = new ArrayList<>();
        token.add(new Field());
        token.add(new Number(5));
        token.add(new Multiplizitaet());
        token.add(new Number(5));
        token.add(new Trenner());
        token.add(new Number(9));

        token.add(new Islands());
        erstelleInsel(token, 0, 0, 3);
        erstelleInsel(token, 0, 2, 4);
        erstelleInsel(token, 0, 4, 2);
        erstelleInsel(token, 2, 0, 3);
        erstelleInsel(token, 2, 3, 2);
        erstelleInsel(token, 3, 2, 1);
        erstelleInsel(token, 3, 4, 1);
        erstelleInsel(token, 4, 0, 3);
        erstelleInsel(token, 4, 3, 3);

        token.add(new Bridges());
        erstelleBruecke(token, 0, 1, true);
        erstelleBruecke(token, 0, 3, false);
        erstelleBruecke(token, 1, 2, false);
        erstelleBruecke(token, 1, 5, false);
        erstelleBruecke(token, 2, 6, false);
        erstelleBruecke(token, 3, 7, true);
        erstelleBruecke(token, 4, 8, true);
        erstelleBruecke(token, 7, 8, false);

        // act
        Spiel spiel = fileService.parseTokenListe(token);

        // assert
        Spiel erwatet = new Spiel(5, 5, 9);
        Insel i1 = new Insel(0, 0, 3);
        erwatet.getInseln().add(i1);
        Insel i2 = new Insel(0, 2, 4);
        erwatet.getInseln().add(i2);
        Insel i3 = new Insel(0, 4, 2);
        erwatet.getInseln().add(i3);
        Insel i4 = new Insel(2, 0, 3);
        erwatet.getInseln().add(i4);
        Insel i5 = new Insel(2, 3, 2);
        erwatet.getInseln().add(i5);
        Insel i6 = new Insel(3, 2, 1);
        erwatet.getInseln().add(i6);
        Insel i7 = new Insel(3, 4, 1);
        erwatet.getInseln().add(i7);
        Insel i8 = new Insel(4, 0, 3);
        erwatet.getInseln().add(i8);
        Insel i9 = new Insel(4, 3, 3);
        erwatet.getInseln().add(i9);

        erwatet.getBruecken().add(new Bruecke(i1, i2, true));
        erwatet.getBruecken().add(new Bruecke(i1, i4, false));
        erwatet.getBruecken().add(new Bruecke(i2, i3, false));
        erwatet.getBruecken().add(new Bruecke(i2, i6, false));
        erwatet.getBruecken().add(new Bruecke(i3, i7, false));
        erwatet.getBruecken().add(new Bruecke(i4, i8, true));
        erwatet.getBruecken().add(new Bruecke(i5, i9, true));
        erwatet.getBruecken().add(new Bruecke(i8, i9, false));

        assertEquals(erwatet, spiel);
    }

    private void erstelleInsel(List<Token> token, int column, int row, int bridges) {
        token.add(new KlammerOffen());
        token.add(new Number(column));
        token.add(new Komma());
        token.add(new Number(row));
        token.add(new Trenner());
        token.add(new Number(bridges));
        token.add(new KlammerZu());
    }

    private void erstelleBruecke(List<Token> token, int startIndex, int endIndex, boolean doppelt) {
        token.add(new KlammerOffen());
        token.add(new Number(startIndex));
        token.add(new Komma());
        token.add(new Number(endIndex));
        token.add(new Trenner());
        token.add(new Boolean(doppelt));
        token.add(new KlammerZu());
    }
}