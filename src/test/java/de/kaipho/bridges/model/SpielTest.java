package de.kaipho.bridges.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class SpielTest {

    @Test
    public void name1() {
        // assert
        Spiel s = new Spiel(22, 21, 10);

        Insel i1 = new Insel(7, 2, 2);
        Insel i2 = new Insel(14, 2, 2);

        Bruecke bruecke = new Bruecke(i1, i2, true);

        s.getInseln().add(i1);
        s.getInseln().add(i2);

        s.getBruecken().add(bruecke);

        // act
        Insel zumEinfuegen = new Insel(10, 2, 1);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i1, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name2() {
        // assert
        Spiel s = new Spiel(22, 21, 10);

        Insel i1 = new Insel(7, 2, 2);
        Insel i2 = new Insel(10, 2, 2);

        Bruecke bruecke = new Bruecke(i1, i2, true);

        s.getInseln().add(i1);
        s.getInseln().add(i2);

        s.getBruecken().add(bruecke);

        // act
        Insel zumEinfuegen = new Insel(14, 2, 1);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i1, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name5() {
        // assert
        Spiel s = new Spiel(22, 21, 10);

        Insel i1 = new Insel(10, 2, 2);
        Insel i2 = new Insel(14, 2, 2);

        Bruecke bruecke = new Bruecke(i1, i2, true);

        s.getInseln().add(i1);
        s.getInseln().add(i2);

        s.getBruecken().add(bruecke);

        // act
        Insel zumEinfuegen = new Insel(7, 2, 1);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i2, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name4() {
        // assert
        Spiel s = new Spiel(22, 21, 10);

        Insel i1 = new Insel(14, 2, 2);
        Insel i2 = new Insel(7, 2, 2);

        Bruecke bruecke = new Bruecke(i1, i2, true);

        s.getInseln().add(i1);
        s.getInseln().add(i2);

        s.getBruecken().add(bruecke);

        // act
        Insel zumEinfuegen = new Insel(10, 2, 1);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i2, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name3() {
        // assert
        Spiel s = new Spiel(22, 21, 10);

        Insel i1 = new Insel(10, 2, 2);
        Insel i2 = new Insel(7, 2, 2);

        Bruecke bruecke = new Bruecke(i1, i2, true);

        s.getInseln().add(i1);
        s.getInseln().add(i2);

        s.getBruecken().add(bruecke);

        // act
        Insel zumEinfuegen = new Insel(14, 2, 1);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i2, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name6() {
        // assert
        Spiel s = new Spiel(15, 22, 10);

        Insel i1 = new Insel(14, 1, 1);
        Insel i2 = new Insel(14, 4, 1);

        Bruecke bruecke = new Bruecke(i1, i2, false);

        s.getInseln().add(i1);
        s.getInseln().add(i2);

        s.getBruecken().add(bruecke);

        // act
        Insel zumEinfuegen = new Insel(14, 13, 2);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i1, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name7() {
        // assert
        Spiel s = new Spiel(15, 22, 10);

        Insel i1 = new Insel(1, 13, 2);
        Insel i2 = new Insel(1, 17, 2);
        Insel i3 = new Insel(1, 20, 2);

        Bruecke bruecke1 = new Bruecke(i1, i2, false);
        Bruecke bruecke2 = new Bruecke(i2, i3, false);

        s.getInseln().add(i1);
        s.getInseln().add(i2);
        s.getInseln().add(i3);

        s.getBruecken().add(bruecke1);
        s.getBruecken().add(bruecke2);

        // act
        Insel zumEinfuegen = new Insel(1, 18, 2);
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i1, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name8() {
        // assert
        Spiel s = new Spiel(15, 22, 10);

        Insel i1 = new Insel(4, 12, 2);
        Insel i2 = new Insel(0, 12, 2);
        Insel i3 = new Insel(2, 15, 2);

        Bruecke bruecke1 = new Bruecke(i1, i2, false);

        s.getInseln().add(i1);
        s.getInseln().add(i2);
        s.getInseln().add(i3);

        s.getBruecken().add(bruecke1);

        // act
        Insel zumEinfuegen = new Insel(2, 0, 2);
        boolean brueckeVorhanden = s.gibtEsEineUeberschneidung(Richtung.NORDEN, i3, zumEinfuegen);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name9() {
        // assert
        Spiel s = new Spiel(15, 22, 10);

        Insel i1 = new Insel(0, 0, 2);
        Insel i2 = new Insel(6, 0, 2);
        Insel i3 = new Insel(3, 4, 2);
        Insel i4 = new Insel(3, 0, 2);

        Bruecke bruecke1 = new Bruecke(i4, i3, false);

        s.getBruecken().add(bruecke1);

        // act
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i1, i2);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name10() {
        // assert
        Spiel s = new Spiel(15, 22, 10);

        Insel i1 = new Insel(4, 10, 2);
        Insel i2 = new Insel(4, 12, 2);
        Insel i3 = new Insel(1, 12, 2);
        Insel i4 = new Insel(9, 12, 2);

        Bruecke bruecke1 = new Bruecke(i3, i4, false);

        s.getBruecken().add(bruecke1);

        // act
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i1, i2);

        // assert
        assertTrue(brueckeVorhanden);
    }

    @Test
    public void name11() {
        // assert
        Spiel s = new Spiel(10, 10, 10);

        Insel i1 = new Insel(8, 7, 2);
        Insel i2 = new Insel(5, 10, 2);
        Insel i3 = new Insel(8, 10, 2);
        Insel i4 = new Insel(10, 10, 2);

        Bruecke bruecke1 = new Bruecke(i1, i3, false);

        s.getBruecken().add(bruecke1);

        // act
        boolean brueckeVorhanden = s.wirdEtwasUeberlagert(i2, i4);

        // assert
        assertTrue(brueckeVorhanden);
    }
}